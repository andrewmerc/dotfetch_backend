package ru.sodateam.dotfetch;

import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableLoadTimeWeaving;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.aspectj.EnableSpringConfigured;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.instrument.classloading.InstrumentationLoadTimeWeaver;

import org.h2.server.web.WebServlet;

/**
 * @author bardyshev
 * @since 27.06.2015
 */
@Configuration
@PropertySource("classpath:application.properties")
// turns on AnnotationBeanConfigurerAspect so that domain objects can be injected with dependencies
@EnableSpringConfigured
// enables load time weaving so that domain objects can be injected with dependencies
@EnableLoadTimeWeaving
public class ApplicationConfiguration {

    /**
     * configures property placeholders, for injections of properties like <code>@Value{$property.name}</code>
     */
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    /**
     * bean used by spring for load time weaving with aspectj
     */
    @Bean
    public InstrumentationLoadTimeWeaver loadTimeWeaver() throws Throwable {
        InstrumentationLoadTimeWeaver loadTimeWeaver = new InstrumentationLoadTimeWeaver();
        return loadTimeWeaver;
    }

    /**
     * for h2 inmemmory db mapping
     */
    @Bean
    ServletRegistrationBean h2servletRegistration() {
        ServletRegistrationBean registrationBean = new ServletRegistrationBean(new WebServlet());
        registrationBean.addUrlMappings("/console/*");
        return registrationBean;
    }

}
