package ru.sodateam.dotfetch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DotFetchApplication {

    public static void main(String[] args) {
        SpringApplication.run(DotFetchApplication.class, args);
    }
}
