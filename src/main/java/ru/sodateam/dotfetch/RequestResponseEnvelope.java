package ru.sodateam.dotfetch;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author bardyshev
 * @since 27.06.2015
 */

public class RequestResponseEnvelope<T> {

    private T data;

    @JsonCreator
    public RequestResponseEnvelope(@JsonProperty("data") T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

}
