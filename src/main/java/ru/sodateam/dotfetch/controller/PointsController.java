package ru.sodateam.dotfetch.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.sodateam.dotfetch.RequestResponseEnvelope;
import ru.sodateam.dotfetch.domain.Point;

/**
 * @author bardyshev
 * @since 27.06.15
 */
@RequestMapping("points")
@RestController
public class PointsController {

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity post(@RequestBody RequestResponseEnvelope<Point> requestBody) {

        Point point = requestBody.getData();

        point.save();

        return ResponseEntity.ok().build();

    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity handleException() {
        return ResponseEntity.badRequest().build();
    }

}
