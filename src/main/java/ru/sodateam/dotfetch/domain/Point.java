package ru.sodateam.dotfetch.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import ru.sodateam.dotfetch.service.PointProximityValidator;
import ru.sodateam.dotfetch.service.PointRepository;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Game point with geocoordinates and player id
 *
 * @author bardyshev
 * @since 27.06.15
 */
@Entity
@Configurable(autowire = Autowire.BY_NAME)
public class Point {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private Long playerId;

    @Column(precision = 7, scale = 4 )
    private BigDecimal longitude;

    @Column(precision = 6, scale = 4 )
    private BigDecimal latitude;

    @Autowired
    @Transient
    private PointRepository pointRepository;

    @Autowired
    @Transient
    private PointProximityValidator pointProximityValidator;

    public Long getId() {
        return id;
    }

    public Long getPlayerId() {
        return playerId;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    Point(){} //JPA constructor

    @JsonCreator
    public Point(@JsonProperty("playerId") Long playerId,
                 @JsonProperty("longitude") BigDecimal longitude,
                 @JsonProperty("latitude") BigDecimal latitude) {
        this(null, playerId, longitude, latitude);
    }

    public Point(Long id, Long playerId, BigDecimal longitude, BigDecimal latitude) {
        this.id = id;
        this.playerId = playerId;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    /**
     * persists point, if it is valid,
     * @return persisted point
     * @throws RuntimeException if validation fails
     */
    public Point save(){
        pointProximityValidator.validate(this);
        return pointRepository.save(this);
    }

}
