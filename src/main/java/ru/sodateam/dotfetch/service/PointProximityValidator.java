package ru.sodateam.dotfetch.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.sodateam.dotfetch.domain.Point;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author bardyshev
 * @since 28.06.2015
 */
@Component
public class PointProximityValidator {

    /**
     * value of point.geocoordinate.proximity.threshold property
     */
    private BigDecimal pointProximityThreshold;

    private PointRepository pointRepository;

    @Autowired
    public PointProximityValidator(@Value("${point.geocoordinate.proximity.threshold}") BigDecimal pointProximityThreshold,
                                   PointRepository pointRepository) {
        this.pointProximityThreshold = pointProximityThreshold;
        this.pointRepository = pointRepository;
    }

    /**
     * Validates that there are no other points with geocoordinates too close to this point
     *
     * @param point
     * @see #pointProximityThreshold
     */
    public void validate(Point point) {

        List<Point> otherPointsCloserThenProximityThreshold = pointRepository.findByCoordinatesWindow(
                point.getLongitude().subtract(pointProximityThreshold), point.getLongitude().add(pointProximityThreshold),
                point.getLatitude().subtract(pointProximityThreshold), point.getLatitude().add(pointProximityThreshold));

        if (otherPointsCloserThenProximityThreshold.isEmpty()) return;

        if (otherPointsCloserThenProximityThreshold.size() == 1) {
            Point otherPoint = otherPointsCloserThenProximityThreshold.get(0);
            if (otherPoint.getId() != null && otherPoint.getId().equals(point.getId())) return;
        }

        throw new RuntimeException("points proximity constraint violated");
    }

}
