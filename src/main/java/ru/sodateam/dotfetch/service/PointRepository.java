package ru.sodateam.dotfetch.service;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ru.sodateam.dotfetch.domain.Point;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author bardyshev
 * @since 27.06.2015
 */
public interface PointRepository extends CrudRepository<Point, Long> {

    // todo
    @Query("select p from Point p where p.longitude >= ?1 and p.longitude <= ?2 and " +
            "p.latitude >= ?3 and p.latitude <= ?4")
    List<Point> findByCoordinatesWindow(BigDecimal longitudeStart, BigDecimal longitudeEnd,
                                              BigDecimal latitudeStart, BigDecimal latitudeEnd);

}
