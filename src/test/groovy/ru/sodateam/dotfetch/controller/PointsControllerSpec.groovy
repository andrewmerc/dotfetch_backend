package ru.sodateam.dotfetch.controller

import groovy.json.JsonOutput
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import ru.sodateam.dotfetch.RequestResponseEnvelope
import ru.sodateam.dotfetch.domain.Point
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup

/**
 * @author bardyshev
 * @since 27.06.2015
 */
class PointsControllerSpec extends Specification {

    def "test post request format"() {

        setup:
        PointsController pointsController = Mock(PointsController)
        MockMvc mockMvc = standaloneSetup(pointsController).build()

        when:
        mockMvc.perform(post("/points").contentType(MediaType.APPLICATION_JSON).content(
                JsonOutput.toJson([
                        data: [
                                playerId : 1,
                                longitude: 50.50g,
                                latitude : 40.50g
                        ]
                ])
        ))

        then:

        // post method should be invoked 1 time and with this arguments
        1 * pointsController.post(_) >> { arguments ->
            RequestResponseEnvelope<Point> parsedRequest = arguments[0]
            assert parsedRequest.data != null
            assert parsedRequest.data.playerId == 1L
            assert parsedRequest.data.longitude == 50.50g
            assert parsedRequest.data.latitude == 40.50g
        }

    }

}
