package ru.sodateam.dotfetch.service

import ru.sodateam.dotfetch.domain.Point
import spock.lang.Specification

/**
 * @author bardyshev
 * @since 28.06.2015
 */
class PointProximityValidatorSpec extends Specification {

    def "validation should pass if no points in proximity threshold"() {

        setup:
        PointRepository pointRepository = Mock(PointRepository)
        // there should be 1 invocation of mock with specified arguments, for which mock should return [] (empty list)
        1 * pointRepository.findByLongitudeBetweenAndLatitudeBetween(50.5004g, 50.5006g, 40.4004g, 40.4006g) >> []

        // when
        new PointProximityValidator(0.0001g, pointRepository).validate(new Point(1, 50.5005g, 40.4005g)); // g is groovy BigDecimal literal

        // then no exceptions

    }

    def "validation should fail if there are other points in proximity threshold"() {

        setup:
        PointRepository pointRepository = Mock(PointRepository)
        // there should be 1 invocation of mock with specified arguments, for which mock should return non empty list
        1 * pointRepository.findByLongitudeBetweenAndLatitudeBetween(50.5004g, 50.5006g, 40.4004g, 40.4006g) >>
                [new Point(1, 50.5004g, 40.4006g)]

        when:
        new PointProximityValidator(0.0001g, pointRepository).validate(new Point(2, 50.5005g, 40.4005g)); // g is groovy BigDecimal literal

        then:
        RuntimeException e = thrown()
        e.message == "points proximity constraint violated"

    }

    def "validation should pass if only point in proximity threshold is this point"() {

        setup:
        PointRepository pointRepository = Mock(PointRepository)
        // there should be 1 invocation of mock with specified arguments, for which mock should return [] (empty list)
        1 * pointRepository.findByLongitudeBetweenAndLatitudeBetween(50.5004g, 50.5006g, 40.4004g, 40.4006g) >> [new Point(1, 1, 50.5005g, 40.4005g)]

        // when
        new PointProximityValidator(0.0001g, pointRepository).validate(new Point(1, 1, 50.5005g, 40.4005g)); // g is groovy BigDecimal literal

        // then no exceptions

    }

}
